//
//  ViewController.swift
//  L5_DZ_2
//
//  Created by Hen Joy on 4/22/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //        drawString(count: 4, osY: 50, osX: 10)
        //        drawLadder(countStep: 3)
        drawPiramid(countStep: 4)
    }
    
    func drawString(count: Int, osY: Int, osX: Int){
        var spaceX = osX
        for _ in 1...count{
            let aim = UIView()
            aim.frame.size.width = 50
            aim.frame.size.height = 50
            aim.frame.origin.x = CGFloat(spaceX)
            aim.frame.origin.y = CGFloat(osY)
            aim.backgroundColor = .cyan
            view.addSubview(aim)
            spaceX += 75
        }
    }
    
    func drawLadder(countStep: Int){
        var y = 50
        for i in 1...countStep{
            drawString(count: i, osY: y, osX: 10)
            y += 75
        }
    }
    
    func drawPiramid(countStep: Int){
        var y = 50 + (76 * countStep)
        var x = 10
        var counter = countStep
        for _ in 1...countStep{
            drawString(count: counter, osY: y, osX: x)
            y -= 76
            counter -= 1
            x += 38
        }
    }
    
}

